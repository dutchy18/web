/**
 * Created by yarel on 16/09/16.
 */
var login = require('./login');
var signup = require('./signup');
var User = require('../models/users');
var mongoose = require('mongoose');
module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        console.log(id)
        User.findById(mongoose.Types.ObjectId(id), function(err, user) {
            console.log('deserializing user:',user);
            done(err, user);
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport);

}