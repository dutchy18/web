/**
 * Created by yarel on 16/09/16.
 */
module.exports = {
    'url' : 'mongodb://localhost/webcourse'
}

var url = 'mongodb://localhost:27017/webcourse';

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var indexRestaurants = function(db, callback) {
    db.collection('users').createIndex(
        { "id": 1 },
        null,
        function(err, results) {
            console.log(results);
            callback();
        }
    );
};

MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    indexRestaurants(db, function() {
        db.close();
    });
});