/**
 * Created by yarel on 16/09/16.
 */

var express = require('express');
var router = express.Router();
var Suspect = require('../models/suspects');
var Record = require('../models/record');
var User = require('../models/users');
var async = require('async');

var perms = require('./perms');


    router.get('/update_suspect_address', perms.isAuthenticated, function (req, res) {
        res.render('update_suspect_address', {})
    });

router.post('/update_suspect_address', perms.isAuthenticated, function (req, res) {
    console.log('here', req.param('street'), req.param('city'));

    Suspect.findOneAndUpdate({'id': req.param('id')},
        {'street': req.param('street'), 'city': req.param('city')}, {upsert: true},
        function (err, doc){
            if (err) {
                return res.send(500)
            }

            return res.send("suspect updated")
        }
    );


});

    router.get('/find_suspects', perms.isAuthenticated, function (req, res) {
        res.render('find_suspects', {})
    });


    router.get('/find_suspect_by_name', perms.isAuthenticated, function (req, res) {

        res.render('find_suspect_by_name', {})
    });
router.get('/insert_suspect', perms.isAuthenticated, function (req, res) {
    res.render('insert_suspect', {})
});



router.get('/find_records_by_suspect', perms.isAuthenticated, function (req, res) {

    res.render('find_records_by_suspect', {})
});

    router.get('/find_suspect_by_id', perms.isAuthenticated, function (req, res) {

        res.render('find_suspect_by_id', {})
    });

router.post('/insert_suspect', perms.isAuthenticated, function (req, res) {
    console.log('insert', req.param('id'));

    var newSuspect = new Suspect();

    // set the user's local credentials
    newSuspect.id = req.param('id');
    newSuspect.first_name = req.param('first_name');
    newSuspect.last_name = req.param('last_name');
    newSuspect.age = req.param('age');
    newSuspect.weight = req.param('weight');
    newSuspect.height = req.param('height');
    newSuspect.city = req.param('city');
    newSuspect.street = req.param('street');
    newSuspect.religion = req.param('religion');
    newSuspect.smoke = req.param('smoke');
    newSuspect.school = req.param('school');


    // save the user
    newSuspect.save(function(err) {
        if (err){
            console.log('Error in Saving user: '+err);
            throw err;
        }
        console.log('User Registration succesful');
        res.send('suspect inserted!')
    });


});


    router.post('/find_suspect_by_id', perms.isAuthenticated, function (req, res) {
        console.log('here', req.param('id'));
        Suspect.find({
            'id': req.param('id')
        }, function (err, suspects) {
            if (err) {
                console.log('cant find suspect');
                res.send('error');
                return
            }

            if (!suspects.length) {
                res.send('no results');
                console.log('no results');

            }
            else {
                console.log('bla', suspects.first_name, suspects.last_name);
                res.json(suspects)
            }
        });
    });


                router.get('/delete_suspect', perms.isAuthenticated, function (req, res) {
                    res.render('delete_suspect', {})
                });

                router.post('/delete_suspect', perms.isAuthenticated, function (req, res) {
                    console.log('delete', req.param('id'));

                    Suspect.remove({'id': req.param('id')},
                        function (err, doc) {
                            if (err) {
                                return res.send(500)
                            }

                            return res.send("suspect deleted!")
                        }
                    );


                });


                router.post('/find_suspect_by_name', perms.isAuthenticated, function (req, res) {
                    console.log('here', req.param('first_name'), req.param('last_name'));
                    Suspect.find({
                        'first_name': req.param('first_name'),
                        'last_name': req.param('last_name')
                    }, function (err, suspects) {
                        if (err) {
                            console.log('cant find suspect');
                            res.send('no results');
                            return
                        }

                        if (!suspects.length) {
                            console.log('no results');

                            res.send('no results');

                        }
                        else {
                            console.log('bla', suspects.first_name, suspects.last_name);
                            res.json(suspects)
                        }
                    });
                });

                router.post('/find_records_by_suspect', perms.isAuthenticated, function (req, res) {
                    console.log('here',  new Date(req.param('mindate')), new Date(req.param('maxdate')), req.param('crimetype'),
                        new Number(req.param('minheight')), new Number(req.param('maxheight')));

                    Suspect.find({
                            "height": {"$gte": req.param('minheight'), "$lt": req.param('maxheight')}
                        }
                    ).populate({
                            path: 'records',
                            match: {
                                "date": {"$gte": new Date(req.param('mindate')), "$lt": new Date(req.param('maxdate'))},
                                "crime_type": req.param('crimetype')
                            }
                        }
                    )
                        .exec(function (err, records) {
                            if (err) {
                                console.log(err);
                                res.send('error');
                                return

                            }

                            records = records.filter(function (record) {
                                return record.records.length > 0; // return only users with records
                            });

                            res.json(records)
                        });

                });


                module.exports = router;
