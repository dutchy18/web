/**
 * Created by yarel on 18/09/16.
 */

var Suspect = require('../models/suspects');
var Record = require('../models/record');
var User = require('../models/users');
var async = require('async');

module.exports = {
    isAuthenticated: function (req, res, next) {
        // if user is authenticated in the session, call the next() to call the next request handler
        // Passport adds this method to request object. A middleware is allowed to add properties to
        // request and response objects
        var calls = [];
        var isValidAccess = false;
        var isAuth = req.isAuthenticated();

        if (isAuth) {
            calls.push(function (callback) {
                User.findOne({'_id': req.user}, function (err, myUser) {
                    if (err) {
                        res.send(500);
                        return
                    }
                    console.log("This is priv: " + myUser.privledges);
                    view = myUser.privledges.view;
                    update = myUser.privledges.update;
                    console.log("This is original url: " + req.originalUrl);
                    callback(null, canAccess(req, view, update));

                });
            });


            async.parallel(calls, function (err, result) {
                if (result == 'true') {
                    return next();
                }
                else if (result == 'false') {
                    res.send(req.user + " doesn't have permissions");
                }
            });
        }
        else {
            res.redirect('/');
        }
    }


};

var canAccess = function (req, view, update) {
    var url = req.originalUrl;

    if (view === 'true' && update === 'true') {
        return 'true';
    }
    else if (url.includes('delete') || url.includes('update') || url.includes('insert')) {
        if (update === 'true') {
            return 'true';
        }
        console.log("doesn't have update returning false");
    }
    else if (url.includes('find') || url.includes('show')) {
        if (view === 'true') {
            return 'true';
        }
        console.log("doesn't have view returning false");
    }

    return 'false';
}