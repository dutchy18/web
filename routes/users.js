var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var User = require('../models/users');
var perms = require('./perms');
var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/');
}

router.get('/delete_user_by_id', perms.isAuthenticated, function (req, res) {

    res.render('delete_user_by_id', {})
});

router.post('/delete_user_by_id', perms.isAuthenticated, function (req, res) {
    console.log('here', req.param('id'));
    User.remove({
        'id': req.param('id')
    }, function (err) {
        if (err) {
            console.log('cant remove user');
            res.send('error');
            return
        }

        res.send('removed')
    });
});

/* GET users listing. */
router.get('/', function(req, res, next) {
    var MongoClient = mongodb.MongoClient;

    var url = 'mongodb://localhost:27017/webcourse'

    MongoClient.connect(url, function (err, db) {
        if(err){
            console.log('unable to connect', err)

        }
        else {
            console.log('connection established');

            var collection = db.collection('users');
            collection.find({}).toArray(function (err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.json(result);
                } else {
                    res.send('no users');
                }

                db.close();
            });
        }

    });

});



module.exports = router;
