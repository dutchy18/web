/**
 * Created by yarel on 16/09/16.
 */

var mongoose = require('mongoose');

module.exports = mongoose.model('Suspect',{
    id: String,
    first_name: String,
    last_name: String,
    age: Number,
    height: Number,
    weight: Number,
    city: String,
    street: String,
    religion: String,
    school: String,
    records: [{type: mongoose.Schema.Types.ObjectId, ref: 'Record'}]

});