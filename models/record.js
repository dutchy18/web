/**
 * Created by yarel on 16/09/16.
 */
var mongoose = require('mongoose');

module.exports = mongoose.model('Record',{
    suspectsids: [{type: mongoose.Schema.Types.ObjectId, ref: 'Suspect'}],
    crime_type: String,
    year: Number,
    verdict: String,
    date: Date
});