/**
 * Created by yarel on 16/09/16.
 */
var mongoose = require('mongoose');
module.exports = mongoose.model('User',{

    username: String,
    password: String,
    email: String,
    first_name: String,
    last_name: String,
    address: String,
    work_place: String,
    rank: String,
    privledges: {
        view: String,
        update: String
    }
});