#!/usr/bin/env bash

mongoimport --db webcourse --collection users --drop --file users_data.json
mongoimport --db webcourse --collection suspects --drop --file suspects.json
mongoimport --db webcourse --collection records --drop --file records.json